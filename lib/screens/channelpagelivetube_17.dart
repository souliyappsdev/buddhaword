import 'package:bottom_bar/screens/search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(Channelpagelivetube17());
}

// ignore: must_be_immutable
class Channelpagelivetube17 extends StatefulWidget {
  @override
  State<Channelpagelivetube17> createState() => _Channelpagelivetube17State();
}

class _Channelpagelivetube17State extends State<Channelpagelivetube17> {
  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        // centerTitle: false,
        title: Text("มูลนิธิอุทยานธรรม",
            style: TextStyle(color: Colors.white), textAlign: TextAlign.left),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goBack();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goForward();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.reload();
              }
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          PageView(
            controller: pageController,
            children: [
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse(
                  "https://www.youtube.com/c/UttayarndhamOrgPage/videos"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              //turn off sound when screen off
              // android: AndroidInAppWebViewOptions(
              //   useHybridComposition: true,
              // ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterTop,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        tooltip: 'ຄົ້ນຫາ',
        elevation: 5,
        child: (Icon(
          Icons.search_sharp,
          size: 34.0,
        )),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SearchScreen()),
          );
        },
      ),
    );
  }
}
