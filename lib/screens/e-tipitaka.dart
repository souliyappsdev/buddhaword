import 'dart:io';
import 'package:bottom_bar/screens/books.dart';
import 'package:bottom_bar/screens/home.dart';
import 'package:bottom_bar/screens/livetube.dart';
import 'package:bottom_bar/screens/meeting.dart';
import 'package:bottom_bar/screens/search.dart';
import 'package:bottom_bar/screens/updatescreen.dart';
import 'package:bottom_bar/sidenav_thai/allbook-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_1-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_10-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_11-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_12-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_13-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_14-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_15-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_16-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_17-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_18-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_19-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_2-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_20-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_21-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_22-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_23-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_24-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_25-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_26-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_27-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_3-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_4-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_5-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_6-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_7-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_8-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_9-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_1-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_10-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_2-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_3-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_4-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_5-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_6-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_7-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_8-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_9-thai.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';
import 'package:permission_handler/permission_handler.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(EtipitakaScreen());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

// ignore: must_be_immutable
class EtipitakaScreen extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  State<EtipitakaScreen> createState() => _EtipitakaScreenState();
}

class _EtipitakaScreenState extends State<EtipitakaScreen> {
  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();

  int navIndex = 0;

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    Permission.camera.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  void initState() {
    widget.browser.addMenuItem(
      new ChromeSafariBrowserMenuItem(
        id: 1,
        label: 'Custom item menu 1',
        action: (url, title) {
          print('Custom item menu 1 clicked!');
        },
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        // centerTitle: true,
        title: Text(
          "E-Tipitaka",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goBack();
              }
            },
          ),
          // IconButton(
          //   color: Colors.white,
          //   icon: Icon(Icons.zoom_out_map_rounded),
          //   iconSize: 32.0,
          //   onPressed: () async {
          //     await widget.browser.open(
          //       url: Uri.parse("https://etipitaka.com/search/"),
          //       options: ChromeSafariBrowserClassOptions(
          //         android: AndroidChromeCustomTabsOptions(
          //           addDefaultShareMenuItem: false,
          //         ),
          //         ios: IOSSafariOptions(
          //           barCollapsingEnabled: true,
          //         ),
          //       ),
          //     );
          //   },
          // ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goForward();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.reload();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.menu_book),
            iconSize: 32.0,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          Builder(
            // ignore: missing_return
            builder: (context) {
              switch (navIndex) {
                case 0:
                  return Center(child: Text('📚ປຶ້ມ ທັງຫມົດ'));
                case 1:
                  return Center(child: Text('📚(ตามรอยธรรม)'));
                case 2:
                  return Center(child: Text('📚(ฆารวาสชั้นเลิศ)'));
                case 3:
                  return Center(child: Text('📚(ก้าวย่าง อย่างพุทธะ)'));
                case 4:
                  return Center(child: Text('📚(มรรควิธีที่ง่าย)'));
                case 5:
                  return Center(child: Text('📚(ทาน)'));
                case 6:
                  return Center(child: Text('📚(ปฐมธรรม)'));
                case 7:
                  return Center(child: Text('📚(คู่มือโสดาบัน)'));
                case 8:
                  return Center(child: Text('📚(อานาปานสติ)'));
                case 9:
                  return Center(child: Text('📚(อินทรียสังวร)'));
                case 10:
                  return Center(child: Text('📚(สาธยายธรรม)'));
                case 11:
                  return Center(child: Text('📚(ภพภูมิ)'));
                case 12:
                  return Center(child: Text('📚(เดรัชฉานวิชา)'));
                case 13:
                  return Center(child: Text('📚(แก้กรรม)'));
                case 14:
                  return Center(child: Text('📚(ตถาคต)'));
                case 15:
                  return Center(child: Text('📚(สมถะ วิปัสสนา)'));
                case 16:
                  return Center(child: Text('📚(สกทาคามี)'));
                case 17:
                  return Center(child: Text('📚(จิต มโน วิญญาณ)'));
                case 18:
                  return Center(child: Text('📚(อนาคามี)'));
                case 19:
                  return Center(child: Text('📚(สัตว์)'));
                case 20:
                  return Center(child: Text('📚(สังโยชน์)'));
                case 21:
                  return Center(child: Text('📚(ปิฎก เล่ม 11)'));
                case 22:
                  return Center(child: Text('📚(อริยวินัย)'));
                case 23:
                  return Center(child: Text('📚(พุทธประวัติจากพระโอษฐ์)'));
                case 24:
                  return Center(child: Text('📚(ขุมทรัพย์จากพระโอษฐ์)'));
                case 25:
                  return Center(
                      child: Text('📚((ภาคต้น)อริยสัจจ์จากพระโอษฐ์)'));
                case 26:
                  return Center(
                      child: Text('📚((ภาคปลาย)อริยสัจจ์จากพระโอษฐ์)'));
                case 27:
                  return Center(child: Text('📚(ปฏิจจสมุปบาทจากพระโอษฐ์)'));
                case 28:
                  return Center(child: Text('📚(แผ่นพับ 10 พระสูตร)'));
                case 29:
                  return Center(child: Text('📚(ผังภพภูมิ)'));
                case 30:
                  return Center(child: Text('📚(ผังภพภูมิ 5 ทั้งคติ)'));
                case 31:
                  return Center(child: Text('📚(ผังอนาคามี)'));
                case 32:
                  return Center(child: Text('📚(ผังอนาคามี กับสะเก็ดไฟ)'));
                case 33:
                  return Center(child: Text('📚(ผังอนาคามีโดยละเอียด)'));
                case 34:
                  return Center(child: Text('📚(ผัง จิต มโน วิญญาณ)'));
                case 35:
                  return Center(child: Text('📚(ปฏิจจสมุปบาท)'));
                case 36:
                  return Center(child: Text('📚(พุทธประวัติ)'));
                case 37:
                  return Center(child: Text('📚(ลำดับการสืบทอดพุทธวจน)'));
                default:
              }
            },
          ),
          PageView(
            controller: pageController,
            children: [
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse("https://etipitaka.com/search/"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,

                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              crossPlatform: InAppWebViewOptions(
                preferredContentMode: UserPreferredContentMode.DESKTOP,
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        tooltip: 'ຄົ້ນຫາ',
        elevation: 5,
        child: (Icon(
          Icons.search_sharp,
          size: 34.0,
        )),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SearchScreen()),
          );
        },
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            // selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.auto_stories,
            '🙏 ພຸດທະ 🙏',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
              );
            },
            // selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.menu_book,
            '📚 E-Tipitaka',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 1);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => EtipitakaScreen()),
              );
            },
            selected: selectedIndex == 1,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.live_tv,
            '📺 Live',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 2);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LivetubeScreen()),
              );
            },
            // selected: selectedIndex == 2,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.meeting_room,
            '📲 Meeting',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 3);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MeetingScreen()),
              );
            },
            // selected: selectedIndex == 3,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.my_library_books_outlined,
            '📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 4);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
            // selected: selectedIndex == 4,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            // selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('📚พุทธวจน',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.my_library_books_sharp, '📚ປຶ້ມ ທັງຫມົດ',
              onTap: () {
            _navItemClicked(context, 0);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AllBookThai()),
            );
          }, selected: selectedIndex == 0),
          Divider(color: Colors.white),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນເບື້ອງຕົ້ນ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ตามรอยธรรม)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book1Thai()),
            );
          }, selected: selectedIndex == 1),
          _navItem(context, Icons.menu_book_sharp, '📚(ฆารวาสชั้นเลิศ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book2Thai()),
            );
          }, selected: selectedIndex == 2),
          _navItem(context, Icons.menu_book_sharp, '📚(ก้าวย่าง อย่างพุทธะ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book3Thai()),
            );
          }, selected: selectedIndex == 3),
          _navItem(context, Icons.menu_book_sharp, '📚(มรรควิธีที่ง่าย)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book4Thai()),
            );
          }, selected: selectedIndex == 4),
          _navItem(context, Icons.menu_book_sharp, '📚(ทาน)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 5);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book5Thai()),
            );
          }, selected: selectedIndex == 5),
          _navItem(context, Icons.menu_book_sharp, '📚(ปฐมธรรม)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 6);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book6Thai()),
            );
          }, selected: selectedIndex == 6),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທ່າມກາງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(คู่มือโสดาบัน)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 7);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book7Thai()),
            );
          }, selected: selectedIndex == 7),
          _navItem(context, Icons.menu_book_sharp, '📚(อานาปานสติ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 8);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book8Thai()),
            );
          }, selected: selectedIndex == 8),
          _navItem(context, Icons.menu_book_sharp, '📚(อินทรียสังวร)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 9);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book9Thai()),
            );
          }, selected: selectedIndex == 9),
          _navItem(context, Icons.menu_book_sharp, '📚(สาธยายธรรม)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 10);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book10Thai()),
            );
          }, selected: selectedIndex == 10),
          _navItem(context, Icons.menu_book_sharp, '📚(ภพภูมิ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 11);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book11Thai()),
            );
          }, selected: selectedIndex == 11),
          _navItem(context, Icons.menu_book_sharp, '📚(เดรัชฉานวิชา)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 12);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book12Thai()),
            );
          }, selected: selectedIndex == 12),
          _navItem(context, Icons.menu_book_sharp, '📚(แก้กรรม)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 13);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book13Thai()),
            );
          }, selected: selectedIndex == 13),
          _navItem(context, Icons.menu_book_sharp, '📚(ตถาคต)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 14);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book14Thai()),
            );
          }, selected: selectedIndex == 14),
          _navItem(context, Icons.menu_book_sharp, '📚(สมถะ วิปัสสนา)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 15);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book15Thai()),
            );
          }, selected: selectedIndex == 15),
          _navItem(context, Icons.menu_book_sharp, '📚(สกทาคามี)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 16);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book16Thai()),
            );
          }, selected: selectedIndex == 16),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທີສຸດ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(จิต มโน วิญญาณ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 17);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book17Thai()),
            );
          }, selected: selectedIndex == 17),
          _navItem(context, Icons.menu_book_sharp, '📚(อนาคามี)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 18);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book18Thai()),
            );
          }, selected: selectedIndex == 18),
          _navItem(context, Icons.menu_book_sharp, '📚(สัตว์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 19);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book19Thai()),
            );
          }, selected: selectedIndex == 19),
          _navItem(context, Icons.menu_book_sharp, '📚(สังโยชน์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 20);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book20Thai()),
            );
          }, selected: selectedIndex == 20),
          _navItem(context, Icons.menu_book_sharp, '📚(ปิฎก เล่ม 11)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 21);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book21Thai()),
            );
          }, selected: selectedIndex == 21),
          _navItem(context, Icons.menu_book_sharp, '📚(อริยวินัย)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 22);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book22Thai()),
            );
          }, selected: selectedIndex == 22),
          _navItem(context, Icons.menu_book_sharp, '📚(พุทธประวัติจากพระโอษฐ์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 23);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book23Thai()),
            );
          }, selected: selectedIndex == 23),
          _navItem(context, Icons.menu_book_sharp, '📚(ขุมทรัพย์จากพระโอษฐ์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 24);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book24Thai()),
            );
          }, selected: selectedIndex == 24),
          _navItem(context, Icons.menu_book_sharp,
              '📚((ภาคต้น)อริยสัจจ์จากพระโอษฐ์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 25);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book25Thai()),
            );
          }, selected: selectedIndex == 25),
          _navItem(context, Icons.menu_book_sharp,
              '📚((ภาคปลาย)อริยสัจจ์จากพระโอษฐ์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 26);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book26Thai()),
            );
          }, selected: selectedIndex == 26),
          _navItem(
              context, Icons.menu_book_sharp, '📚(ปฏิจจสมุปบาทจากพระโอษฐ์)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 27);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book27Thai()),
            );
          }, selected: selectedIndex == 27),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ເເຜນຜັງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(แผ่นพับ 10 พระสูตร)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 28);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram1Thai()),
            );
          }, selected: selectedIndex == 28),
          _navItem(context, Icons.menu_book_sharp, '📚(ผังภพภูมิ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 29);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram2Thai()),
            );
          }, selected: selectedIndex == 29),
          _navItem(context, Icons.menu_book_sharp, '📚(ผังภพภูมิ 5 ทั้งคติ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 30);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram3Thai()),
            );
          }, selected: selectedIndex == 30),
          _navItem(context, Icons.menu_book_sharp, '📚(ผังอนาคามี)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 31);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram4Thai()),
            );
          }, selected: selectedIndex == 31),
          _navItem(context, Icons.menu_book_sharp, '📚(ผังอนาคามี กับสะเก็ดไฟ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 32);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram5Thai()),
            );
          }, selected: selectedIndex == 32),
          _navItem(context, Icons.menu_book_sharp, '📚(ผังอนาคามีโดยละเอียด)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 33);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram6Thai()),
            );
          }, selected: selectedIndex == 33),
          _navItem(context, Icons.menu_book_sharp, '📚(ผัง จิต มโน วิญญาณ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 34);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram7Thai()),
            );
          }, selected: selectedIndex == 34),
          _navItem(context, Icons.menu_book_sharp, '📚(ปฏิจจสมุปาท)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 35);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram8Thai()),
            );
          }, selected: selectedIndex == 35),
          _navItem(context, Icons.menu_book_sharp, '📚(พุทธประวัติ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 36);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram9Thai()),
            );
          }, selected: selectedIndex == 36),
          _navItem(context, Icons.menu_book_sharp, '📚(ลำดับการสืบทอดพุทธวจน)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 37);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram10Thai()),
            );
          }, selected: selectedIndex == 37),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text suffix, Function onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}
