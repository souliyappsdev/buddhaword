import 'dart:core';
import 'dart:async';
import 'dart:io';
import 'package:bottom_bar/screens/books.dart';
import 'package:bottom_bar/screens/channelvdo_page.dart';
import 'package:bottom_bar/screens/e-tipitaka.dart';
import 'package:bottom_bar/screens/home.dart';
import 'package:bottom_bar/screens/meeting.dart';
import 'package:bottom_bar/screens/search.dart';
import 'package:bottom_bar/screens/updatescreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';
import 'package:scroll_app_bar/scroll_app_bar.dart';
import 'package:flutter/widgets.dart';
import 'package:permission_handler/permission_handler.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    WidgetsFlutterBinding.ensureInitialized();
    await Permission.camera.request();
    await Permission.microphone.request();
    await Permission.storage.request();

    if (Platform.isAndroid) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }
    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
          AndroidServiceWorkerController.instance();

      serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async {
          print(request);
          return null;
        },
      );
    }
  }

  runApp(LivetubeScreen());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

// ignore: must_be_immutable
class LivetubeScreen extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  State<LivetubeScreen> createState() => _LivetubeScreenState();
}

class _LivetubeScreenState extends State<LivetubeScreen> {
  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();

  final controllerscroll = ScrollController();

  final ScrollController scrollcontroller = new ScrollController();

  // ignore: unused_field
  ScrollController _scrollViewController;

  // ignore: unused_field
  bool _showAppbar = true;

  bool isScrollingDown = false;

  TransformationController controllers = TransformationController();

  String velocity = "VELOCITY";
  int navIndex = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    Permission.camera.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: ScrollAppBar(
        controller: controllerscroll,
        backgroundColor: Colors.transparent,
        elevation: 0,
        // centerTitle: true,
        title: Text(
          "Live",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goBack();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goForward();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.reload();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.menu_book),
            iconSize: 32.0,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.loose,
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          PageView(
            controller: pageController,
            children: <Widget>[
              Builder(
                // ignore: missing_return
                builder: (context) {
                  switch (navIndex) {
                    case 0:
                      return Center(child: Text('🔎ຄົ້ນຫາ'));
                    case 1:
                      return Center(child: Text('🙏 ພຸດທະ 🙏'));
                    case 2:
                      return Center(child: Text('📚 E-Tipitaka'));
                    case 3:
                      return Center(child: Text('📺 Live'));
                    case 4:
                      return Center(child: Text('📲 Meeting'));
                    case 5:
                      return Center(child: Text('📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ'));
                    case 6:
                      return Center(child: Text('📲 ປັບປຸງລະບົບ'));
                    default:
                  }
                },
              ),
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            children: [
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse(
                  "https://sites.google.com/view/livebuddhaword/live"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,

                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              //turn off sound when screen off
              // android: AndroidInAppWebViewOptions(
              //   useHybridComposition: true,
              // ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        tooltip: 'ຄົ້ນຫາ',
        elevation: 5,
        child: (Icon(
          Icons.video_collection_rounded,
          size: 34.0,
        )),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ChannelPageScreen()),
          );
        },
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.auto_stories, '🙏 ພຸດທະ 🙏',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
            );
          }, selected: selectedIndex == 1),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.menu_book, '📚 E-Tipitaka',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EtipitakaScreen()),
            );
          }, selected: selectedIndex == 2),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.live_tv, '📺 Live',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LivetubeScreen()),
            );
          }, selected: selectedIndex == 3),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.meeting_room, '📲 Meeting',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MeetingScreen()),
            );
          }, selected: selectedIndex == 4),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.my_library_books_outlined,
            '📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 5);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
            selected: selectedIndex == 5,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text suffix, Function onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}



// Code Add VDO to GoogleSite
// <iframe class="latestVideoEmbed" vnum='0' cid="UCDp1PB9hJZAdOVHpKqvnpJw" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='1' cid="UCY6EKdDvA2lqu7ol6giO0EQ" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='2' cid="UCoMlAzNrjjSBOXJKyxaAfbA" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='3' cid="UCmmvbpbGkVuHvdiiYbVRj2A" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='4' cid="UCKAv48NZM7RPtw7DmY9ReRg" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='5' cid="UC53gu2Q0VFaeJQJAjIVBE3A" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='6' cid="UCzmwQXDzVFhYX3yz8mi_31w" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='7' cid="UC9ECnZyAst8YrNHQ8KZthKQ" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='8' cid="UClG5qgd2ofx0i4HUuZ64WDg" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UCt47gsmBzLqlFS4gVDcImVQ" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UC16iUneS7E3a8RSw8XzI_9w" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UC4wqFWTOy2Pu8VpLE-5jWDA" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UCHCEeOHZx16sbpo9-I6E3gw" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UCevwityEQT6dlXk1SYN_RPg" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UCfos2v7ANVBaw29i2KWlQpQ" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UC-5x0bouEoThXb-JjGYxdhw" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>
// <iframe class="latestVideoEmbed" vnum='9' cid="UCAJa-dmq3HPJtZTSgkArTNw" width="310" height="250" frameborder="0" allowfullscreen></iframe><p>&nbsp;</p>

// <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
// <script>
// var reqURL = "https://api.rss2json.com/v1/api.json?rss_url=" + encodeURIComponent("https://www.youtube.com/feeds/videos.xml?channel_id=");
// function loadVideo(iframe) {
//   $.getJSON(reqURL + iframe.getAttribute('cid'),
//     function(data) {
//       var videoNumber = (iframe.getAttribute('vnum') ? Number(iframe.getAttribute('vnum')) : 0);
//       console.log(videoNumber);
//       var link = data.items[videoNumber].link;
//       id = link.substr(link.indexOf("=") + 1);
//       iframe.setAttribute("src", "https://youtube.com/embed/" + id + "?controls=0");
//     }
//   );
// }
// var iframes = document.getElementsByClassName('latestVideoEmbed');
// for (var i = 0, len = iframes.length; i < len; i++) {
//   loadVideo(iframes[i]);
// }
// </script>