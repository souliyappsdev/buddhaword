import 'dart:core';
import 'dart:async';
import 'dart:io';
import 'package:bottom_bar/screens/books.dart';
import 'package:bottom_bar/screens/e-tipitaka.dart';
import 'package:bottom_bar/screens/livetube.dart';
import 'package:bottom_bar/screens/meeting.dart';
import 'package:bottom_bar/screens/search.dart';
import 'package:bottom_bar/screens/updatescreen.dart';
import 'package:bottom_bar/sidenav_lao/allbook-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_1-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_2-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_3-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_4-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_5-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_6-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_7-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_8-lao.dart';
import 'package:bottom_bar/sidenav_lao/diagram_1-lao.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';
import 'package:scroll_app_bar/scroll_app_bar.dart';
import 'package:flutter/widgets.dart';
import 'package:permission_handler/permission_handler.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  runApp(HomeScreen());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class HomeScreen extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();

  final controllerscroll = ScrollController();

  final ScrollController scrollcontroller = new ScrollController();

  // ignore: unused_field
  ScrollController _scrollViewController;

  // ignore: unused_field
  bool _showAppbar = true;

  bool isScrollingDown = false;

  TransformationController controllers = TransformationController();

  String velocity = "VELOCITY";

  ScrollController _scrollController =
      new ScrollController(); // set controller on scrolling
  bool _show = true;

  @override
  void initState() {
    widget.browser.addMenuItem(
      new ChromeSafariBrowserMenuItem(
        id: 1,
        label: 'Custom item menu 1',
        action: (url, title) {
          print('Custom item menu 1 clicked!');
        },
      ),
    );
    super.initState();
    addItemsToTheList();
    handleScroll();
  }

  @override
  void dispose() {
    _scrollController.removeListener(() {});
    super.dispose();
  }

  void showFloationButton() {
    setState(() {
      _show = true;
    });
  }

  void hideFloationButton() {
    setState(() {
      _show = false;
    });
  }

  void addItemsToTheList() {}

  void handleScroll() async {
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        hideFloationButton();
      }
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        showFloationButton();
      }
    });
  }

  int navIndex = 0;

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    Permission.camera.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [SystemUiOverlay.bottom]);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: ScrollAppBar(
        controller: controllerscroll,
        backgroundColor: Colors.transparent,
        elevation: 0,
        // centerTitle: true,
        title: Text(
          "ພຸດທະ",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goBack();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.ios_share),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse(
                    "https://sites.google.com/view/buddhawajana/%E0%BA%84%E0%BA%B3%E0%BA%AA%E0%BA%AD%E0%BA%99%E0%BA%9E%E0%BA%A3%E0%BA%B0%E0%BA%9E%E0%BA%94%E0%BA%97%E0%BA%B0%E0%BB%80%E0%BA%88%E0%BA%B2?authuser=0"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goForward();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () async {
              // You can request multiple permissions at once.
              Map<Permission, PermissionStatus> statuses = await [
                Permission.microphone,
                Permission.camera,
                //add more permission to request here.
              ].request();

              if (statuses[Permission.microphone].isDenied) {
                //check each permission status after.
                print("Microphone permission is denied.");
              }

              if (statuses[Permission.camera].isDenied) {
                //check each permission status after.
                print("Camera permission is denied.");
              }
              if (webView != null) {
                webView.reload();
                webView.clearCache();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.menu_book),
            iconSize: 32.0,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.loose,
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          Builder(
            // ignore: missing_return
            builder: (context) {
              switch (navIndex) {
                case 0:
                  return Center(child: Text('📚ປຶ້ມ ທັງຫມົດ'));
                case 1:
                  return Center(child: Text('📚(ຄະຣາວາສຊັ້ນເລີດ)'));
                case 2:
                  return Center(child: Text('📚(ຄູ່ມືໂສດາບັນ)'));
                case 3:
                  return Center(child: Text('📚(ທານ)'));
                case 4:
                  return Center(child: Text('📚(ສາທະຍາຍທັມ)'));
                case 5:
                  return Center(child: Text('📚(ພຸດທະວະຈະນະ(ໂດຍພາບລວມ))'));
                case 6:
                  return Center(child: Text('📚(ແກ້ກັມ)'));
                case 7:
                  return Center(child: Text('📚(ສະຕິປັດຖານ 4)'));
                case 8:
                  return Center(child: Text('📚(ອິດທິບາດ 4)'));
                case 9:
                  return Center(child: Text('📚(ປະຕິຈະສະນຸບາດ)'));
                default:
              }
            },
          ),
          PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            children: [
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse(
                  "https://sites.google.com/view/buddhawajana/%E0%BA%84%E0%BA%B3%E0%BA%AA%E0%BA%AD%E0%BA%99%E0%BA%9E%E0%BA%A3%E0%BA%B0%E0%BA%9E%E0%BA%94%E0%BA%97%E0%BA%B0%E0%BB%80%E0%BA%88%E0%BA%B2?authuser=0"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                  _scrollController = controller as ScrollController;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,

                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
                preferredContentMode: UserPreferredContentMode.DESKTOP,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      floatingActionButton: Visibility(
        visible: _show,
        child: FloatingActionButton(
          backgroundColor: Colors.brown[400],
          tooltip: 'ຄົ້ນຫາ',
          elevation: 1,
          child: (Icon(
            Icons.search_sharp,
            size: 34.0,
          )),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SearchScreen()),
            );
          },
        ),
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            // selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.auto_stories,
            '🙏 ພຸດທະ 🙏',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
              );
            },
            // selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.menu_book,
            '📚 E-Tipitaka',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 1);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => EtipitakaScreen()),
              );
            },
            selected: selectedIndex == 1,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.live_tv,
            '📺 Live',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 2);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LivetubeScreen()),
              );
            },
            // selected: selectedIndex == 2,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.meeting_room,
            '📲 Meeting',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 3);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MeetingScreen()),
              );
            },
            // selected: selectedIndex == 3,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.my_library_books_outlined,
            '📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 4);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
            // selected: selectedIndex == 4,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              // _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            // selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('📚ປຶ້ມ ຄຳສອນພຣະພຸດທະເຈົ້າ',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.my_library_books_sharp, '📚ປຶ້ມ ທັງຫມົດ',
              onTap: () {
            _navItemClicked(context, 0);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AllBookLao()),
            );
          }, selected: selectedIndex == 0),
          Divider(color: Colors.white),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນເບື້ອງຕົ້ນ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ຄະຣາວາສຊັ້ນເລີດ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book1Lao()),
            );
          }, selected: selectedIndex == 1),
          _navItem(context, Icons.menu_book_sharp, '📚(ຄູ່ມືໂສດາບັນ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book2Lao()),
            );
          }, selected: selectedIndex == 2),
          _navItem(context, Icons.menu_book_sharp, '📚(ທານ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book3Lao()),
            );
          }, selected: selectedIndex == 3),
          _navItem(context, Icons.menu_book_sharp, '📚(ສາທະຍາຍທັມ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book4Lao()),
            );
          }, selected: selectedIndex == 4),
          _navItem(context, Icons.menu_book_sharp, '📚(ພຸດທະວະຈະນະ(ໂດຍພາບລວມ))',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 5);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book5Lao()),
            );
          }, selected: selectedIndex == 5),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທ່າມກາງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ແກ້ກັມ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 6);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book6Lao()),
            );
          }, selected: selectedIndex == 6),
          _navItem(context, Icons.menu_book_sharp, '📚(ສະຕິປັດຖານ 4)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 7);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book7Lao()),
            );
          }, selected: selectedIndex == 7),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ທຳໃນທີສຸດ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ອິດທິບາດ 4)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 8);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Book8Lao()),
            );
          }, selected: selectedIndex == 8),
          Padding(
            padding: EdgeInsets.all(10.10),
            child: Text('ເເຜນຜັງ',
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    letterSpacing: 1)),
          ),
          _navItem(context, Icons.menu_book_sharp, '📚(ປະຕິຈະສະມຸບາດ)',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 9);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Diagram1Lao()),
            );
          }, selected: selectedIndex == 9),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text suffix, Function onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}
