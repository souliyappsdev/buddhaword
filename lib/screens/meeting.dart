import 'dart:io';
import 'dart:async';
import 'package:bottom_bar/screens/books.dart';
import 'package:bottom_bar/screens/e-tipitaka.dart';
import 'package:bottom_bar/screens/home.dart';
import 'package:bottom_bar/screens/livetube.dart';
import 'package:bottom_bar/screens/search.dart';
import 'package:bottom_bar/screens/updatescreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:permission_handler/permission_handler.dart';
// import 'package:bottom_bar/screens/jitsi_meet.dart';
// import 'jitsi_meet.dart';
// import 'package:flutter_downloader/flutter_downloader.dart';
// import 'package:path_provider/path_provider.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }

  runApp(MaterialApp(home: MeetingScreen()));
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class MeetingScreen extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  _MeetingScreenState createState() => new _MeetingScreenState();
}

class _MeetingScreenState extends State<MeetingScreen> {
  @override
  void initState() {
    widget.browser.addMenuItem(new ChromeSafariBrowserMenuItem(
        id: 1,
        label: 'Custom item menu 1',
        action: (url, title) {
          print('Custom item menu 1 clicked!');
        }));
    super.initState();
  }

  double _progress = 0;
  InAppWebViewController webView;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();
  int navIndex = 0;
  final controllerscroll = ScrollController();

  final ScrollController scrollcontroller = new ScrollController();

  // ignore: unused_field
  ScrollController _scrollViewController;
  // ignore: unused_field
  bool _showAppbar = true;
  bool isScrollingDown = false;

  TransformationController controllers = TransformationController();
  String velocity = "VELOCITY";

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        // title: const Text('Meeting'),
        actions: <Widget>[
          // Padding(
          //   padding: EdgeInsets.only(right: 5),
          //   child: IconButton(
          //     color: Colors.white,
          //     icon: Icon(Icons.arrow_back),
          //     iconSize: 32.0,
          //     onPressed: () {
          //       if (webView != null) {
          //         webView.goBack();
          //       }
          //     },
          //   ),
          // ),
          Padding(
            padding: EdgeInsets.only(right: 0),
            child: ElevatedButton.icon(
              label: Text('Call'),
              icon: Icon(Icons.video_call_rounded),
              style: ElevatedButton.styleFrom(
                primary: Colors.brown[300],
                onPrimary: Colors.white,
                shadowColor: Colors.brown[400],
                elevation: 5,
                textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal,
                ),
              ),
              onPressed: () async {
                // ignore: unnecessary_statements
                webViewMethod;
                await widget.browser.open(
                    url: Uri.parse("https://meet.jit.si/buddhaword"),
                    options: ChromeSafariBrowserClassOptions(
                        android: AndroidChromeCustomTabsOptions(
                            addDefaultShareMenuItem: false),
                        ios: IOSSafariOptions(barCollapsingEnabled: true)));
              },
            ),
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () async {
              // You can request multiple permissions at once.
              Map<Permission, PermissionStatus> statuses = await [
                Permission.microphone,
                Permission.camera,
                //add more permission to request here.
              ].request();

              if (statuses[Permission.microphone].isDenied) {
                //check each permission status after.
                print("Microphone permission is denied.");
              }

              if (statuses[Permission.camera].isDenied) {
                //check each permission status after.
                print("Camera permission is denied.");
              }
              if (webView != null) {
                webView.reload();
              }
            },
          ),
          // Padding(
          //   padding: EdgeInsets.only(right: 50.0),
          //   child: ElevatedButton.icon(
          //     label: Text('VDO Call'),
          //     icon: Icon(Icons.video_call_rounded),
          //     style: ElevatedButton.styleFrom(
          //       primary: Colors.brown[300],
          //       onPrimary: Colors.white,
          //       shadowColor: Colors.brown[400],
          //       elevation: 5,
          //       textStyle: TextStyle(
          //         color: Colors.black,
          //         fontSize: 20,
          //         fontStyle: FontStyle.normal,
          //       ),
          //     ),
          //     onPressed: () async {
          //       await Navigator.push(
          //         context,
          //         MaterialPageRoute(builder: (context) => Meeting()),
          //       );
          //     },
          //   ),
          // ),
          Padding(
            padding: EdgeInsets.only(right: 50),
            child: ElevatedButton.icon(
              label: Text('Chat'),
              icon: Icon(Icons.chat_rounded),
              style: ElevatedButton.styleFrom(
                // minimumSize: const Size(100, 50),
                // maximumSize: const Size(100, 50),
                primary: Colors.amber[600],
                onPrimary: Colors.black,
                shadowColor: Colors.brown[400],
                elevation: 5,
                textStyle: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontStyle: FontStyle.normal,
                ),
              ),
              // https://tawk.to/chat/61763b9bf7c0440a591fc969/1fir1aar5
              // https://discord.gg/WAmmd76Y
              onPressed: () async {
                await widget.browser.open(
                  url: Uri.parse(
                      "https://tawk.to/chat/61763b9bf7c0440a591fc969/1fir1aar5"),
                  options: ChromeSafariBrowserClassOptions(
                    android: AndroidChromeCustomTabsOptions(
                        addDefaultShareMenuItem: false),
                    ios: IOSSafariOptions(barCollapsingEnabled: true),
                  ),
                );
              },
            ),
          ),
          // ElevatedButton.icon(
          //   label: Text('Chat'),
          //   icon: Icon(Icons.chat_rounded),
          //   style: ElevatedButton.styleFrom(
          //     primary: Colors.amber[600],
          //     onPrimary: Colors.black,
          //     shadowColor: Colors.brown[400],
          //     elevation: 5,
          //     textStyle: TextStyle(
          //       color: Colors.black,
          //       fontSize: 20,
          //       fontStyle: FontStyle.normal,
          //     ),
          //   ),
          //   onPressed: () async {
          //     await widget.browser.open(
          //         url: Uri.parse("https://discord.gg/WAmmd76Y"),
          //         options: ChromeSafariBrowserClassOptions(
          //             android: AndroidChromeCustomTabsOptions(
          //                 addDefaultShareMenuItem: false),
          //             ios: IOSSafariOptions(barCollapsingEnabled: true)));
          //   },
          // ),
          // Padding(
          //   padding: EdgeInsets.only(right: 5),
          //   child:

          // ),
        ],
      ),
      resizeToAvoidBottomInset: true,
      body: Stack(
        // overflow: Overflow.clip,
        // controller: controllerscroll.appBar,
        fit: StackFit.loose,
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          PageView(
            controller: pageController,
            children: <Widget>[
              Builder(
                // ignore: missing_return
                builder: (context) {
                  switch (navIndex) {
                    case 0:
                      return Center(child: Text('🔎ຄົ້ນຫາ'));
                    case 1:
                      return Center(child: Text('🙏 ພຸດທະ 🙏'));
                    case 2:
                      return Center(child: Text('📚 E-Tipitaka'));
                    case 3:
                      return Center(child: Text('📺 Live'));
                    case 4:
                      return Center(child: Text('📲 Meeting'));
                    case 5:
                      return Center(child: Text('📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ'));
                    case 6:
                      return Center(child: Text('📲 ປັບປຸງລະບົບ'));
                    default:
                  }
                },
              ),
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          // ignore: deprecated_member_use
          RaisedButton(
            onPressed: webViewMethod,
            child: Text('Join'),
            textColor: Colors.black,
          ),
          PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            children: [
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          // InteractiveViewer(
          //   boundaryMargin: EdgeInsets.all(0.0),
          //   minScale: 1.0,
          //   maxScale: 2.5,
          //   scaleEnabled: true,
          //   panEnabled: true,
          //   transformationController: _transformationController,
          //   onInteractionEnd: (ScaleEndDetails endDetails) {
          //     print(endDetails);
          //     print(endDetails.velocity);
          //     velocity = endDetails.velocity.toString();
          //     double scale =
          //         _transformationController.value.getMaxScaleOnAxis();
          //   },
          // Zoom(
          //   maxZoomWidth: 385,
          //   // ignore: deprecated_member_use
          //   height: 2700,
          //   zoomSensibility: 1.0,
          //   initZoom: 1.0,
          //   opacityScrollBars: 0.2,
          //   centerOnScale: true,
          //   doubleTapZoom: true,
          //   enableScroll: true,

          // child:

          InAppWebView(
            initialUrlRequest: URLRequest(
              url: Uri.parse(
                  "https://calendar.google.com/calendar/embed?src=souliyappsdev%40gmail.com&ctz=Asia%2FBangkok"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },

            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              //turn off sound when screen off
              // android: AndroidInAppWebViewOptions(
              //   useHybridComposition: true,
              // ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
                // debuggingEnabled: true,
              ),
            ),

            // onDownloadStart: (controller, url) async {
            //   print("onDownloadStart $url");
            //   WidgetsFlutterBinding.ensureInitialized();
            //   await FlutterDownloader.initialize(debug: true);
            //   // ignore: unused_local_variable
            //   final taskId = await FlutterDownloader.enqueue(
            //     // url: url.toString(),
            //     url:
            //         'https://sites.google.com/view/buddhawajana/%E0%BA%84%E0%BA%B3%E0%BA%AA%E0%BA%AD%E0%BA%99%E0%BA%9E%E0%BA%A3%E0%BA%B0%E0%BA%9E%E0%BA%94%E0%BA%97%E0%BA%B0%E0%BB%80%E0%BA%88%E0%BA%B2?authuser=0',
            //     savedDir: (await getExternalStorageDirectory()).path,
            //     showNotification:
            //         true, // show download progress in status bar (for Android)
            //     openFileFromNotification:
            //         true, // click on notification to open downloaded file (for Android)
            //   );
            // },
            // ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        tooltip: 'ຄົ້ນຫາ',
        elevation: 5,
        child: (Icon(
          Icons.search_sharp,
          size: 34.0,
        )),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SearchScreen()),
          );
        },
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.auto_stories, '🙏 ພຸດທະ 🙏',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
            );
          }, selected: selectedIndex == 1),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.menu_book, '📚 E-Tipitaka',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EtipitakaScreen()),
            );
          }, selected: selectedIndex == 2),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.live_tv, '📺 Live',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LivetubeScreen()),
            );
          }, selected: selectedIndex == 3),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.meeting_room, '📲 Meeting',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MeetingScreen()),
            );
          }, selected: selectedIndex == 4),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.my_library_books_outlined,
            '📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 5);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
            selected: selectedIndex == 5,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text suffix, Function onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}
