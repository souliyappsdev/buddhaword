// import 'package:bottom_bar/screens/home.dart';
import 'dart:io';
import 'dart:async';
import 'package:bottom_bar/screens/books.dart';
import 'package:bottom_bar/screens/e-tipitaka.dart';
import 'package:bottom_bar/screens/home.dart';
import 'package:bottom_bar/screens/livetube.dart';
import 'package:bottom_bar/screens/meeting.dart';
import 'package:bottom_bar/screens/updatescreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:flutter_local_notifications/src/platform_specifics/android/notification_channel.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    // "This channel is used for important notifications.", // description
    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('A bg message just showed up :  ${message.messageId}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    WidgetsFlutterBinding.ensureInitialized();
    await Permission.camera.request();
    await Permission.microphone.request();
    await Permission.storage.request();

    if (Platform.isAndroid) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }
    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
          AndroidServiceWorkerController.instance();

      serviceWorkerController.serviceWorkerClient = AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async {
          print(request);
          return null;
        },
      );
    }
  }

  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  runApp(SearchScreen());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class SearchScreen extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  State<SearchScreen> createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  // int _counter = 0;

  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();
  int navIndex = 0;

  // @override
  // void initState() {
  //   super.initState();
  // }

  @override
  void dispose() {
    super.dispose();
  }

  Future webViewMethod() async {
    print('In Microphone permission method');
    WidgetsFlutterBinding.ensureInitialized();

    Permission.microphone.request();
    Permission.camera.request();
    await WebViewMethodForCamera();
  }

  // ignore: non_constant_identifier_names
  Future WebViewMethodForCamera() async {
    print('In Camera permission method');
    WidgetsFlutterBinding.ensureInitialized();
    Permission.camera.request();
    Permission.microphone.request();
  }

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                // channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/launcher_icon',
              ),
            ));
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        showDialog(
            context: context,
            builder: (_) {
              return AlertDialog(
                title: Text(notification.title),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [Text(notification.body)],
                  ),
                ),
              );
            });
      }
    });
  }

  // void showNotification() {
  //   setState(() {
  //     _counter++;
  //   });
  //   flutterLocalNotificationsPlugin.show(
  //       0,
  //       "Testing $_counter",
  //       "How you doin ?",
  //       NotificationDetails(
  //           android: AndroidNotificationDetails(channel.id, channel.name,
  //               // channel.description,
  //               importance: Importance.high,
  //               color: Colors.blue,
  //               playSound: true,
  //               icon: '@mipmap/launcher_icon')));
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      drawer: Sidenav(navIndex, (int index) {
        setState(() {
          navIndex = index;
        });
      }),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        // centerTitle: true,
        title: Text(
          "",
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_back),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goBack();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.ios_share),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse(
                    "https://www.appsheet.com/start/5480d3a7-719a-4436-9de6-8da95283b56e"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.arrow_forward),
            iconSize: 32.0,
            onPressed: () {
              if (webView != null) {
                webView.goForward();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.refresh),
            iconSize: 32.0,
            onPressed: () async {
              // showNotification();
              // You can request multiple permissions at once.
              Map<Permission, PermissionStatus> statuses = await [
                Permission.microphone,
                Permission.camera,
                //add more permission to request here.
              ].request();

              if (statuses[Permission.microphone].isDenied) {
                //check each permission status after.
                print("Microphone permission is denied.");
              }

              if (statuses[Permission.camera].isDenied) {
                //check each permission status after.
                print("Camera permission is denied.");
              }
              if (webView != null) {
                webView.reload();
              }
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.mode_edit),
            iconSize: 32.0,
            onPressed: () async {
              await widget.browser.open(
                url: Uri.parse(
                    "https://www.appsheet.com/start/7d6156a4-7476-4882-8355-f742d49a01a6"),
                options: ChromeSafariBrowserClassOptions(
                  android: AndroidChromeCustomTabsOptions(
                    addDefaultShareMenuItem: false,
                  ),
                  ios: IOSSafariOptions(
                    barCollapsingEnabled: true,
                  ),
                ),
              );
            },
          ),
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.menu_book),
            iconSize: 32.0,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Books()),
              );
            },
          ),
        ],
      ),
      body: Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          PageView(
            controller: pageController,
            children: <Widget>[
              Builder(
                // ignore: missing_return
                builder: (context) {
                  switch (navIndex) {
                    case 0:
                      return Center(child: Text('🔎 ຄົ້ນຫາ'));
                    case 1:
                      return Center(child: Text('🙏 ພຸດທະ 🙏'));
                    case 2:
                      return Center(child: Text('📚 E-Tipitaka'));
                    case 3:
                      return Center(child: Text('📺 Live'));
                    case 4:
                      return Center(child: Text('📲 Meeting'));
                    case 5:
                      return Center(child: Text('📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ'));
                    case 6:
                      return Center(child: Text('📲 ປັບປຸງລະບົບ'));

                    default:
                  }
                },
              ),
              Container(
                color: Colors.brown,
              ),
            ],
          ),
          InAppWebView(
            initialUrlRequest: URLRequest(
              // https://buddhaword.siteoly.com/
              // https://www.appsheet.com/start/5480d3a7-719a-4436-9de6-8da95283b56e
              url: Uri.parse(
                  "https://www.appsheet.com/start/5480d3a7-719a-4436-9de6-8da95283b56e"),
            ),
            androidOnPermissionRequest: (InAppWebViewController controller,
                String origin, List<String> resources) async {
              return PermissionRequestResponse(
                  resources: resources,
                  action: PermissionRequestResponseAction.GRANT);
            },
            onWebViewCreated: (InAppWebViewController controller) {
              webView = controller;
            },
            onReceivedServerTrustAuthRequest: (controller, challenge) async {
              return ServerTrustAuthResponse(
                  action: ServerTrustAuthResponseAction.PROCEED);
            },
            onProgressChanged:
                (InAppWebViewController controller, int progress) {
              setState(
                () {
                  _progress = progress / 100;
                },
              );
            },
            initialOptions: InAppWebViewGroupOptions(
              ios: IOSInAppWebViewOptions(
                allowsInlineMediaPlayback: true,
                limitsNavigationsToAppBoundDomains:
                    true, // adds Service Worker API on iOS 14.0+
              ),
              //turn off sound when screen off
              // android: AndroidInAppWebViewOptions(
              //   useHybridComposition: true,
              // ),
              crossPlatform: InAppWebViewOptions(
                mediaPlaybackRequiresUserGesture: false,
                useOnDownloadStart: true,
                useShouldOverrideUrlLoading: true,
                supportZoom: true,
              ),
            ),
          ),
          _progress < 1
              ? SizedBox(
                  height: 3,
                  child: LinearProgressIndicator(
                    value: _progress,
                    backgroundColor:
                        // ignore: deprecated_member_use
                        Theme.of(context).accentColor.withOpacity(0.2),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }
}

class Sidenav extends StatelessWidget {
  final Function setIndex;
  final int selectedIndex;

  Sidenav(this.selectedIndex, this.setIndex);
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Menu',
                style: TextStyle(
                    color: Theme.of(context).bottomAppBarColor, fontSize: 21)),
          ),
          Divider(color: Colors.white),
          _navItem(
            context,
            Icons.search,
            '🔎 ຄົ້ນຫາ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 0);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchScreen()),
              );
            },
            selected: selectedIndex == 0,
          ),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.auto_stories, '🙏 ພຸດທະ 🙏',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 1);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
            );
          }, selected: selectedIndex == 1),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.menu_book, '📚 E-Tipitaka',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 2);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EtipitakaScreen()),
            );
          }, selected: selectedIndex == 2),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.live_tv, '📺 Live',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 3);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LivetubeScreen()),
            );
          }, selected: selectedIndex == 3),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.meeting_room, '📲 Meeting',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 4);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MeetingScreen()),
            );
          }, selected: selectedIndex == 4),
          Divider(color: Colors.grey.shade400),
          _navItem(context, Icons.my_library_books_outlined,
              '📖 ປື້ມ 🔊 ສຽງ 🎥 ວີດີໂອ',
              suffix: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.w500),
              ), onTap: () {
            _navItemClicked(context, 5);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Books()),
            );
          }, selected: selectedIndex == 5),
          Divider(color: Colors.grey.shade400),
          _navItem(
            context,
            Icons.system_update_alt_sharp,
            '📲 ປັບປຸງລະບົບ',
            suffix: Text(
              '',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            onTap: () {
              _navItemClicked(context, 6);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UpdateScreen()),
              );
            },
            selected: selectedIndex == 6,
          ),
          Divider(color: Colors.grey.shade400),
        ],
      ),
    );
  }

  _navItem(BuildContext context, IconData icon, String text,
          {Text suffix, Function onTap, bool selected = false}) =>
      Container(
        color: selected ? Colors.brown[300] : Colors.transparent,
        child: ListTile(
          leading: Icon(icon,
              color: selected ? Theme.of(context).canvasColor : Colors.white),
          trailing: suffix,
          title: Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          ),
          selected: selected,
          onTap: onTap,
        ),
      );

  _navItemClicked(BuildContext context, int index) {
    setIndex(index);
    Navigator.of(context).pop();
  }
}
