// import 'package:bottom_bar/screens/live.dart';
import 'package:bottom_bar/screens/channelpagelive_1.dart';
import 'package:bottom_bar/screens/channelpagelive_10.dart';
import 'package:bottom_bar/screens/channelpagelive_11.dart';
import 'package:bottom_bar/screens/channelpagelive_12.dart';
import 'package:bottom_bar/screens/channelpagelive_13.dart';
import 'package:bottom_bar/screens/channelpagelive_14.dart';
import 'package:bottom_bar/screens/channelpagelive_15.dart';
import 'package:bottom_bar/screens/channelpagelive_16.dart';
import 'package:bottom_bar/screens/channelpagelive_17.dart';
import 'package:bottom_bar/screens/channelpagelive_2.dart';
import 'package:bottom_bar/screens/channelpagelive_3.dart';
import 'package:bottom_bar/screens/channelpagelive_4.dart';
import 'package:bottom_bar/screens/channelpagelive_5.dart';
import 'package:bottom_bar/screens/channelpagelive_6.dart';
import 'package:bottom_bar/screens/channelpagelive_7.dart';
import 'package:bottom_bar/screens/channelpagelive_8.dart';
import 'package:bottom_bar/screens/channelpagelive_9.dart';
import 'package:bottom_bar/screens/channelpagelivetube_18.dart';
import 'package:flutter/material.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(ChannelPageScreen());
}

class ChannelPageScreen extends StatefulWidget {
  ChannelPageScreen({Key key}) : super(key: key);

  @override
  _ChannelPageScreenState createState() => _ChannelPageScreenState();
}

class _ChannelPageScreenState extends State<ChannelPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.brown,
        // leading: Padding(
        //   padding: const EdgeInsets.only(left: 12.0),
        //   child: Image.asset(
        //     'assets/ypf.png',
        //     fit: BoxFit.fitWidth,
        //   ),
        // ),
        title: const Text(
          'Channel Video',
          style: TextStyle(
            color: Colors.white,
            backgroundColor: Colors.brown,
            fontSize: 30,
          ),
        ),
        // actions: [
        //   IconButton(
        //     color: Colors.white,
        //     icon: Icon(Icons.video_call_outlined),
        //     iconSize: 32.0,
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //   ),
        // ],
      ),
      body: Container(
        // height: 100,
        color: Colors.brown[300],
        child: ListView(
          padding: const EdgeInsets.all(20.0),
          children: <Widget>[
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/Oldpathtohappiness.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text("Old path to happiness"),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive1()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ພຸທທະພົຈນ໌.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 140, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ພຸທທະພົຈນ໌",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive2()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ຕະຖາຄົຕພາສິຕ.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 100, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ຕະຖາຄົຕ ພາສິຕ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive3()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/KatiyaAnothai.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 90, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "Katiya Anothai",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive4()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจนเรียล.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 110, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "พุทธวจนเรียล",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive5()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/DhammaStudy.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 80, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "Dhamma Study",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive6()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ธรรมทาน.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 160, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ธรรมทาน",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive7()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/สั่งสมสุตตะ.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 140, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สั่งสมสุตตะ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive8()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/มูลนิธิพุทธโฆษณ์.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 70, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "มูลนิธิพุทธโฆษณ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive9()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/Buddhawajana.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 90, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "Buddhawajana",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive10()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ภันเตธานินทร์.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 110, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ภันเตธานินทร์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive11()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/สมาธิ.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 200, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สมาธิ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive12()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/Makkanuka08.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 90, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "Makkanuka 08",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive13()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ธรรมยังไง.jpg",
                width: 70,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 130, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ธรรม ยังไง?",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive14()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/nirdukkha.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 150, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "nirdukkha",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive15()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ทางนิพพาน.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 135, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ทางนิพพาน",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive16()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/มูลนิธิอุทยานธรรม.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 60, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "มูลนิธิอุทยานธรรม",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ChannelpageLive17()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ธรรมสุขวิหาร.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 110, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ธรรมสุขวิหาร",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Channelpagelivetube18()),
                );
              },
            ),
          ],
        ),
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.brown[400],
      //   tooltip: 'Live Video',
      //   elevation: 5,
      //   child: (Icon(Icons.video_collection_rounded)),
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(builder: (context) => LiveScreen()),
      //     );
      //   },
      // ),
    );
  }
}
