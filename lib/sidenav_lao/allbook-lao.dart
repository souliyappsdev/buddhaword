// import 'package:bottom_bar/screens/channelpagelive_10.dart';
// import 'package:bottom_bar/screens/channelpagelive_11.dart';
// import 'package:bottom_bar/screens/channelpagelive_12.dart';
// import 'package:bottom_bar/screens/channelpagelive_13.dart';
// import 'package:bottom_bar/screens/channelpagelive_14.dart';
// import 'package:bottom_bar/screens/channelpagelive_15.dart';
// import 'package:bottom_bar/screens/channelpagelive_16.dart';
// import 'package:bottom_bar/screens/channelpagelive_17.dart';
// import 'package:bottom_bar/screens/channelpagelive_6.dart';
// import 'package:bottom_bar/screens/channelpagelive_8.dart';
// import 'package:bottom_bar/screens/channelpagelive_9.dart';
// import 'package:bottom_bar/screens/channelpagelivetube_18.dart';

import 'package:bottom_bar/sidenav_lao/book_1-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_2-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_3-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_4-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_5-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_6-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_7-lao.dart';
import 'package:bottom_bar/sidenav_lao/book_8-lao.dart';
import 'package:bottom_bar/sidenav_lao/diagram_1-lao.dart';
import 'package:flutter/material.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(AllBookLao());
}

class AllBookLao extends StatefulWidget {
  AllBookLao({Key key}) : super(key: key);

  @override
  _AllBookLaoPageState createState() => _AllBookLaoPageState();
}

class _AllBookLaoPageState extends State<AllBookLao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.brown,
        // leading: Padding(
        //   padding: const EdgeInsets.only(left: 12.0),
        //   child: Image.asset(
        //     'assets/ypf.png',
        //     fit: BoxFit.fitWidth,
        //   ),
        // ),
        title: const Text(
          '📚ປຶ້ມ ທັງຫມົດ',
          style: TextStyle(
            color: Colors.white,
            backgroundColor: Colors.brown,
            fontSize: 30,
          ),
        ),
        // actions: [
        //   IconButton(
        //     color: Colors.white,
        //     icon: Icon(Icons.video_call_outlined),
        //     iconSize: 32.0,
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //   ),
        // ],
      ),
      body: Container(
        // height: 100,
        color: Colors.brown[300],
        child: ListView(
          padding: const EdgeInsets.all(20.0),
          children: <Widget>[
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ຄະຣາວາສຊັ້ນເລີດ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 90, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text("ຄະຣາວາສຊັ້ນເລີດ"),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book1Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ຄູ່ມືໂສດາບັນ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 145, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ຄູ່ມືໂສດາບັນ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book2Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ທານ.png",
                width: 70,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(0, 5, 220, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ທານ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book3Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ສາທະຍາຍທັມ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 125, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ສາທະຍາຍທັມ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book4Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/แผ่นพับ_10_พระสูตร.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 40, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ພຸດທະວະຈະນະ(ໂດຍພາບລວມ)",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book5Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ແກ້ກັມ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 200, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ແກ້ກັມ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book6Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ສະຕິປັຕຖານ.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 120, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ສະຕິປັຕຖານ 4",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book7Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ອິດທິບາດ_໔_ສະບັບປັບປຸງ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 150, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ອິດທິບາດ 4",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book8Lao()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/แผ่นพับ_10_พระสูตร.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 90, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ປະຕິຈະສະມຸບາດ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram1Lao()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
