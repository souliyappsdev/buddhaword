// import 'package:bottom_bar/screens/channelpagelive_10.dart';
// import 'package:bottom_bar/screens/channelpagelive_11.dart';
// import 'package:bottom_bar/screens/channelpagelive_12.dart';
// import 'package:bottom_bar/screens/channelpagelive_13.dart';
// import 'package:bottom_bar/screens/channelpagelive_14.dart';
// import 'package:bottom_bar/screens/channelpagelive_15.dart';
// import 'package:bottom_bar/screens/channelpagelive_16.dart';
// import 'package:bottom_bar/screens/channelpagelive_17.dart';
// import 'package:bottom_bar/screens/channelpagelive_6.dart';
// import 'package:bottom_bar/screens/channelpagelive_8.dart';
// import 'package:bottom_bar/screens/channelpagelive_9.dart';
// import 'package:bottom_bar/screens/channelpagelivetube_18.dart';

import 'package:bottom_bar/sidenav_thai/book_1-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_10-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_11-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_12-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_13-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_14-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_15-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_16-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_17-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_18-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_19-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_2-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_20-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_21-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_22-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_23-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_24-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_25-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_26-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_27-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_3-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_4-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_5-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_6-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_7-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_8-thai.dart';
import 'package:bottom_bar/sidenav_thai/book_9-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_1-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_10-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_2-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_3-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_4-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_5-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_6-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_7-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_8-thai.dart';
import 'package:bottom_bar/sidenav_thai/diagram_9-thai.dart';
import 'package:flutter/material.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(AllBookThai());
}

class AllBookThai extends StatefulWidget {
  AllBookThai({Key key}) : super(key: key);

  @override
  _AllBookThaiPageState createState() => _AllBookThaiPageState();
}

class _AllBookThaiPageState extends State<AllBookThai> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.brown,
        // leading: Padding(
        //   padding: const EdgeInsets.only(left: 12.0),
        //   child: Image.asset(
        //     'assets/ypf.png',
        //     fit: BoxFit.fitWidth,
        //   ),
        // ),
        title: const Text(
          '📚ປຶ້ມ ທັງຫມົດ',
          style: TextStyle(
            color: Colors.white,
            backgroundColor: Colors.brown,
            fontSize: 30,
          ),
        ),
        // actions: [
        //   IconButton(
        //     color: Colors.white,
        //     icon: Icon(Icons.video_call_outlined),
        //     iconSize: 32.0,
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //   ),
        // ],
      ),
      body: Container(
        // height: 100,
        color: Colors.brown[300],
        child: ListView(
          padding: const EdgeInsets.all(20.0),
          children: <Widget>[
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_1_ตามรอยธรรม.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 120, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text("ตามรอยธรรม"),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book1Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_2_ฆารวาสชั้นเลิศ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 110, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ฆารวาสชั้นเลิศ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book2Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_3_ก้าวย่าง_อย่างพุทธะ.png",
                width: 70,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(0, 5, 50, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ก้าวย่าง อย่างพุทธะ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book3Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_4_มรรควิธีที่ง่าย.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 125, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "มรรควิธีที่ง่าย",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book4Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_5_ทาน.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 225, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ทาน",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book5Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_6_ปฐมธรรม.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 170, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ปฐมธรรม",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book6Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_7_คู่มือโสดาบัน.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 135, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "คู่มือโสดาบัน",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book7Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_8_อานาปานสติ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 135, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "อานาปานสติ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book8Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_9_อินทรียสังวร.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 135, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "อินทรียสังวร",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book9Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_10_สาธยายธรรม.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 120, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สาธยายธรรม",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book10Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_11_ภพภูมิ.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 200, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ภพภูมิ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book11Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_12_เดรัชฉานวิชา.jpg",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 120, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "เดรัชฉานวิชา",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book12Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_13_แก้กรรม.png",
                // width: 50,
                // height: 50,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 175, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "แก้กรรม",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book13Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_14_ตถาคต.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 185, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ตถาคต",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book14Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_15_สมถะ_วิปัสสนา.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 105, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สมถะ วิปัสสนา",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book15Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_16_สกทาคามี.png",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 155, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สกทาคามี",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book16Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_17_จิต_มโน_วิญญาณ.png",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 75, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "จิต มโน วิญญาณ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book17Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_18_อนาคามี.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 170, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "อนาคามี",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book18Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_19_สัตว์.png",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 215, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สัตว์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book19Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน_20_สังโยชน์.png",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 165, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "สังโยชน์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book20Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธวจน-ปิฎก_เล่ม_11.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 120, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ปิฎก เล่ม 11",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book21Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/อริยวินัย.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 165, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "อริยวินัย",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book22Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธประวัติจากพระโอษฐ์.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 25,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(20, 5, 105, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "พุทธประวัติจากพระโอษฐ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book23Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ขุมทรัพย์จากพระโอษฐ์.jpg",
                width: 80,
                height: 80,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 25,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(4, 5, 123, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ขุมทรัพย์จากพระโอษฐ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book24Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ภาคต้น_อริยสัจจ์จากพระโอษฐ์.jpg",
                width: 80,
                height: 80,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 25,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(4, 5, 35, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "(ภาคต้น)อริยสัจจ์จากพระโอษฐ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book25Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ภาคปลาย_อริยสัจจ์จากพระโอษฐ์.jpg",
                width: 80,
                height: 80,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 25,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(4, 5, 35, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "(ภาคปลาย)อริยสัจจ์จากพระโอษฐ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book26Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ปฏิจจสมุปบาทจากพระโอษฐ์.jpg",
                width: 80,
                height: 80,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 25,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(4, 5, 60, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ปฏิจจสมุปบาทจากพระโอษฐ์",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Book27Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/แผ่นพับ_10_พระสูตร.jpg",
                // width: 70,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 36, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "แผ่นพับ 10 พระสูตร",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram1Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผังภพภูมิ.png",
                width: 100,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 116, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผังภพภูมิ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram2Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผังภพภูมิ_5_ทั้งคติ.jpg",
                // width: 100,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 80, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผังภพภูมิ 5 ทั้งคติ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram3Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผังอนาคามี.png",
                // width: 100,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 150, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผังอนาคามี",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram4Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผังอนาคามี_กับสะเก็ดไฟ.png",
                // width: 100,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 5, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผังอนาคามี กับสะเก็ดไฟ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram5Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผังอนาคามีโดยละเอียด.png",
                // width: 100,
                // height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 15, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผังอนาคามีโดยละเอียด",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram6Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ผัง_จิต_มโน_วิญญาณ.jpg",
                width: 100,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 100, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ผัง จิต มโน วิญญาณ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram7Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ปฏิจจสมุปบาท.jpg",
                width: 100,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(1, 5, 70, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ปฏิจจสมุปบาท",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram8Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/พุทธประวัติ.jpg",
                width: 100,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 110, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "พุทธประวัติ",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram9Thai()),
                );
              },
            ),
            SizedBox(height: 8.0),
            ElevatedButton.icon(
              icon: new Image.asset(
                "assets/ลำดับการสืบทอดพุทธวจน.png",
                width: 100,
                height: 70,
              ),
              style: ElevatedButton.styleFrom(
                textStyle: TextStyle(
                  fontSize: 27,
                  fontStyle: FontStyle.normal,
                ),
                fixedSize: const Size(240, 80),
                padding: const EdgeInsets.fromLTRB(5, 5, 40, 5),
                primary: Colors.brown,
                shadowColor: Colors.amber[600],
                elevation: 6,
                side: BorderSide(color: Colors.amber[600], width: 1),
                shape: const BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
              ),
              label: Text(
                "ลำดับการสืบทอดพุทธวจน",
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print('Pressed');
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Diagram10Thai()),
                );
              },
            ),
          ],
        ),
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.brown[400],
      //   tooltip: 'Live Video',
      //   elevation: 5,
      //   child: (Icon(Icons.video_collection_rounded)),
      //   onPressed: () {
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(builder: (context) => LiveScreen()),
      //     );
      //   },
      // ),
    );
  }
}
