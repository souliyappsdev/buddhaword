import 'dart:isolate';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter/rendering.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.microphone.request();
  await Permission.storage.request();
  await FlutterDownloader.initialize();
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: true // optional: set false to disable printing logs to console
      );
  runApp(Diagram5Thai());
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  @override
  void onOpened() {
    print("ChromeSafari browser opened");
  }

  @override
  void onCompletedInitialLoad() {
    print("ChromeSafari browser initial load completed");
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
  }
}

class Diagram5Thai extends StatefulWidget {
  final ChromeSafariBrowser browser = new MyChromeSafariBrowser();

  @override
  _Diagram5ThaiState createState() => _Diagram5ThaiState();
}

class _Diagram5ThaiState extends State<Diagram5Thai> {
  double _progress = 0;

  InAppWebViewController webView;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  PDFDocument document;

  Future download(String url) async {
    var status = await Permission.storage.request();
    if (status.isGranted) {
      final baseStorage = await getExternalStorageDirectory();
      await FlutterDownloader.enqueue(
        url: url,
        savedDir: baseStorage.path,
        showNotification:
            true, // show download progress in status bar (for Android)
        openFileFromNotification:
            true, // click on notification to open downloaded file (for Android)
      );
    }
  }

  ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();
    widget.browser.addMenuItem(new ChromeSafariBrowserMenuItem(
        id: 1,
        label: 'Custom item menu 1',
        action: (url, title) {
          print('Custom item menu 1 clicked!');
        }));
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      // ignore: unused_local_variable
      String id = data[0];
      DownloadTaskStatus status = data[1];
      // ignore: unused_local_variable
      int progress = data[2];
      if (status == DownloadTaskStatus.complete) {
        print('Download Completed!');
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        canvasColor: Colors.brown[400],
        fontFamily: 'Georgia',
        textTheme: const TextTheme(
          bodyText2: TextStyle(fontSize: 30.0, fontFamily: 'Georgia'),
        ),
      ),
      home: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.brown[400]),
          backgroundColor: Colors.brown,
          elevation: 0,
          // centerTitle: true,
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20),
              child: IconButton(
                color: Colors.white,
                icon: Icon(Icons.arrow_back),
                iconSize: 32.0,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Text(
                "ผังอนาคามี กับสะเก็ดไฟ",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  height: 1.8,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(left: 0),
                child: IconButton(
                  color: Colors.white,
                  icon: Icon(Icons.file_download),
                  iconSize: 32.0,
                  onPressed: () async {
                    await widget.browser.open(
                      url: Uri.parse(
                          "https://fv2-1.failiem.lv/down.php?i=cm35nbqxf&download_checksum=39007367dc78e0685dc7cd9d2b9c3767ea412855&download_timestamp=1634016942"),
                      options: ChromeSafariBrowserClassOptions(
                        android: AndroidChromeCustomTabsOptions(
                            addDefaultShareMenuItem: false),
                        ios: IOSSafariOptions(barCollapsingEnabled: true),
                      ),
                    );
                  },
                )),
          ],
        ),
        body: Stack(
          fit: StackFit.passthrough,
          children: <Widget>[
            PageView(
              controller: pageController,
              children: <Widget>[
                Container(
                  color: Colors.brown,
                ),
              ],
            ),
            InAppWebView(
              initialUrlRequest: URLRequest(
                url: Uri.parse(
                    "http://www.anakame.com/image/Brochure/Big_103.gif"),
              ),
              androidOnPermissionRequest: (InAppWebViewController controller,
                  String origin, List<String> resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              onWebViewCreated: (InAppWebViewController controller) {
                webView = controller;
              },
              onReceivedServerTrustAuthRequest: (controller, challenge) async {
                return ServerTrustAuthResponse(
                    action: ServerTrustAuthResponseAction.PROCEED);
              },
              onProgressChanged:
                  (InAppWebViewController controller, int progress) {
                setState(
                  () {
                    _progress = progress / 100;
                  },
                );
              },
              initialOptions: InAppWebViewGroupOptions(
                ios: IOSInAppWebViewOptions(
                  allowsInlineMediaPlayback: true,
                  limitsNavigationsToAppBoundDomains:
                      true, // adds Service Worker API on iOS 14.0+
                ),
                crossPlatform: InAppWebViewOptions(
                  mediaPlaybackRequiresUserGesture: false,
                  useOnDownloadStart: true,
                  useShouldOverrideUrlLoading: true,
                  supportZoom: true,
                ),
              ),
            ),
            _progress < 1
                ? SizedBox(
                    height: 3,
                    child: LinearProgressIndicator(
                      value: _progress,
                      backgroundColor:
                          // ignore: deprecated_member_use
                          Theme.of(context).accentColor.withOpacity(0.2),
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
