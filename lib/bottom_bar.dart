import 'package:bottom_bar/screens/home.dart';
import 'package:bottom_bar/screens/e-tipitaka.dart';
import 'package:bottom_bar/screens/livetube.dart';
import 'package:bottom_bar/screens/search.dart';
// import 'package:bottom_bar/screens/live.dart';
import 'package:bottom_bar/screens/meeting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'screens/search.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Mobile Safari/537.36';

class BottomBarScreen extends StatefulWidget {
  static const routeName = '/BottomBarScreen';

  @override
  _BottomBarScreenState createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  List _pages = [
    SearchScreen(),
    HomeScreen(),
    EtipitakaScreen(),
    LivetubeScreen(),
    MeetingScreen(),
  ];

  // List<Map<String, Object>> _pages;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _pages = [
      {'page': SearchScreen(), 'title': 'Search Screen'},
      {'page': HomeScreen(), 'title': 'ຄຳສອນພຣະພຸດທະເຈົ້າ'},
      {'page': EtipitakaScreen(), 'title': 'E-Tipitaka'},
      {'page': LivetubeScreen(), 'title': 'Live'},
      {"page": MeetingScreen(), 'title': 'Meeting'},
    ];
  }

  final int aNullableInt = null;
  bool get mounted;

  void _selectedPage(int index) async {
    if (this.mounted) {
      setState(() {
        _selectedIndex = index;
        super.activate();
      });
    }
  }

  // ignore: unused_field
  double _progress = 0;

  InAppWebViewController webView;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  InAppWebViewController controller;

  get bottomNavigationBar => null;

  PageController pageController = PageController();

  get tabs => null;

  TextEditingController searchController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      key: scaffoldKey,
      body: _pages[_selectedIndex]['page'],
      bottomNavigationBar: BottomAppBar(
        notchMargin: 3,
        clipBehavior: Clip.antiAlias,
        // elevation: 5,
        shape: CircularNotchedRectangle(),
        child: Container(
          // height: kBottomNavigationBarHeight * 0.8,
          decoration: BoxDecoration(
              border: Border(
            top: BorderSide(width: 0.5),
          )),
          child: BottomNavigationBar(
            onTap: _selectedPage,
            backgroundColor: Theme.of(context).primaryColor,
            //  backgroundColor: Colors.transparent,
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.grey,
            currentIndex: _selectedIndex,
            items: [
              BottomNavigationBarItem(
                activeIcon: null,
                icon:
                    // Icon(null),
                    Icon(
                  Icons.search,
                  // color: Colors.transparent,
                ),
                tooltip: 'ຄົ້ນຫາ',
                label: 'ຄົ້ນຫາ',
              ),
              BottomNavigationBarItem(
                icon: new Image.asset("assets/buddhaLogo.png"),
                tooltip: 'ພຸດທະ',
                label: 'ພຸດທະ',
              ),
              BottomNavigationBarItem(
                icon: new Image.asset("assets/etipitaka.png"),
                tooltip: 'E-Tipitaka',
                label: 'E-Tipitaka',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.live_tv_rounded),
                tooltip: 'Live',
                label: 'Live',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.chat_outlined),
                tooltip: 'Meeting',
                label: 'Meeting',
              ),
            ],
          ),
        ),
      ),

      // floatingActionButtonLocation:
      //     FloatingActionButtonLocation.miniCenterDocked,
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Colors.brown[400],
      //   tooltip: 'ຄົ້ນຫາ',
      //   elevation: 5,
      //   child: (Icon(Icons.search)),
      //   onPressed: () {
      //     setState(() {
      //       _selectedIndex = 2;
      //       // Navigator.push(
      //       //   context,
      //       //   MaterialPageRoute(builder: (context) => SearchScreen()),
      //       // );
      //     });
      //   },
      // ),
    );
  }
}
